﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel; //used for ObservableCollection
using System.Threading.Tasks;
using Xamarin.Forms;

namespace lab2
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]

    public class TwoImagesTwoTexts 
    {
        public string Text1 { get; set; }
        public string Text2 { get; set; }
        public ImageSource Image1 { get; set; }
        public ImageSource Image2 { get; set; }
    }
    public partial class MainPage : ContentPage
    {
        ObservableCollection<TwoImagesTwoTexts> listOfTextAndImages;
        public MainPage()
        {
            InitializeComponent();
            listOfTextAndImages = new ObservableCollection<TwoImagesTwoTexts>();
            PopulateImagesAndText();
        }
        private void PopulateImagesAndText ()
        {
            
            listOfTextAndImages.Add(new TwoImagesTwoTexts 
            
                {Text1="Before",
                Text2="After",
                Image1= "before.jfif" ,
                Image2= "after.jpg"
            });
            listOfTextAndImages.Add(new TwoImagesTwoTexts
            {
                Text1 = "Before",
                Text2 = "After",
                Image1 = "egg.jpg",
                Image2 = "chicken.jpg"
            });

            ListView1.ItemsSource = listOfTextAndImages;
        }
    }
}
